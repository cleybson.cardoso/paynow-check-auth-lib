const test = require('ava');
const auth = require('.');

test('Iniciando sem inicializar a aplicação', async (t) => {
  auth.init('http://127.0.0.1:3000/v1/check-token', 'teste');
  await auth.check(
    { headers: { authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRshIjp7ImlkIjoiMDFhNDhhZDMtYWQ2Mi00NjI2LWFmNDYtYTBhYzMzZDdkY2NlIiwibmFtZSI6InRlc3RlIiwiZW1haWwiOiJ0ZXN0ZTJAZ21haWwuY29tIiwidHlwZSI6ImNsaWVudCIsInRlbmFudHMiOlsiY29sb21ibyJdfSwianRpIjoiJDJiJDEwJG41bnNTMHQ0UFZsakswNDFCcE5uNXUvVUhHMVJOLlY0bzgyUi5TVFdCUG5welNMT1RDSldlIiwiaWF0IjoxNjM0NzczMTQwLCJleHAiOjE2MzQ3NzQ5NDB9.7rmM_uTwnEBUkVaVqGD2-Vq4ARjsh134_9CYa4tb1wU' }},
    { status: (n) => ({ json: (v) => false }) },
    () => true
  );
})