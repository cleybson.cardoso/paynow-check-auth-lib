const axios = require('axios').default;
let urlUsed = "";
let tenantUsed = "";

const init = (url, tenant) => {
  urlUsed = url;
  tenantUsed = tenant;
}

const check = async (req, res, next) => {
  if (!urlUsed || !tenantUsed) {
    throw new Error('Inicialize o paynow auth');
  }
  try {
    const response = await axios({
      url: urlUsed,
      headers: {
        authorization: req.headers.authorization
      }
    });
    req.user = response.data.data;
    next();
  } catch (err) {
    if (err.response && err.response.status) {
      return res.status(err.response.status).json(err.response.data);
    } else {
      return res.status(401).json({ success: false, message: 'Token invalido' })
    }
  }
}

const auth = {
  init,
  check
}

module.exports = auth;
